<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;

/**
 * Description of Main
 *
 * @author lyjen
 */
class Main extends DefaultController {
   public function __construct(){
        parent::__construct();
    }
    
    public function helloFramework(){
        echo 'Hello Framework, Rueckgrat';
    }
}
